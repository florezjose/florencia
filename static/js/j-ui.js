/**
 * JUI Template basic
 *
 * @link    https://joseflorez.co
 * @license For open source use: MIT
 * @author  Jose Florez https://joseflorez.co
 * @version 1.0.0
 *
 *
 */


class JUITemplate {
    constructor(options) {
        this.options = options;
    }

    mount() {
        if (this.options.includes("form")) {
            document.querySelectorAll(".j-field").forEach(el => {
                this.addContentInputField(el);
            })
        }
        if (this.options.includes("menu")) {
            document.querySelectorAll("ul.menu .toggle").forEach(el => {
                el.addEventListener('click', () => {
                    this.toggleMenu(el);
                })
            })
        }
        if (this.options.includes("alerts")) {
            document.querySelectorAll('.alert .close').forEach(el => {
                el.addEventListener("click", (el) => {
                    this.closeAlerts(el);
                });
            })
        }
    }

    addContentInputField(el) {
        let tag = document.createElement("span");
        el.appendChild(tag);
    }

    toggleMenu(el) {
        el.classList.toggle('open')
        let sub = el.parentElement.nextElementSibling;
        let height = sub.clientHeight === 0 ? sub.scrollHeight : 0;
        sub.style.height = `${height}px`;
    }

    closeAlerts(e) {
        let el = e.path.find(e => e.classList.contains('alert'))
        if (el) el.style.display = "none";
    }
}

const JUI = {
    ui(options) {
        return new JUITemplate(options)
    }
}

const {ui} = JUI;
ui(["form", "menu", "alerts"]).mount();

//
let els = document.querySelectorAll('.copy-data')
console.log(els)
els.forEach(item => {
    item.addEventListener('click', function (el) {
        let clipboard = new ClipboardJS(`#${el.currentTarget.getAttribute('id')}`);
        let alert = document.getElementById("alerts")
        clipboard.on('success', function (e) {
            alert.innerHTML = `
                        <div class="alert success">
                            <div class="text">Enlace de formulario copiado</div>
                        </div>
                    `
        });

        setTimeout(function () {
            alert.innerHTML = '';
        }, 2000)
    })
})
