from django.contrib.auth import authenticate, login
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.contrib import messages
from django.views import View
from django.utils.translation import ugettext_lazy as _


def redirect_session(request):
    if request.user.is_authenticated:
        return redirect(reverse_lazy('questions:questions_box'))

    return redirect(reverse_lazy('core:sign_in'))


class SignInView(View):
    template_name = "core/sign_in.html"

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect(reverse_lazy('core:redirect_session'))
        return render(request, self.template_name)

    def post(self, request, *args, **kwargs):
        email = request.POST.get('email', None)
        password = request.POST.get('password', None)
        get_user = User.objects.filter(email__exact=email).first()
        if get_user:
            user = authenticate(request, username=get_user.username, password=password)
            if user is not None:
                login(request, user)
                return redirect(reverse_lazy('core:redirect_session'))
            else:
                messages.error(request, _('Correo o contraseña incorrecta. ¡Inténtelo de nuevo!'))
        else:
            messages.error(request, _('Correo o contraseña incorrecta. ¡Inténtelo de nuevo!'))
        return redirect(reverse_lazy('core:sign_in'))


class DashboardView(LoginRequiredMixin, View):
    template_name = "core/dashboard.html"

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)

    def post(self, request, *args, **kwargs):
        pass
