from django.urls import path
from django.contrib.auth.views import LogoutView

from core import views
from florencia import settings

app_name = 'core'
urlpatterns = [
    path('', views.SignInView.as_view(), name='sign_in'),
    # path('sign-up', views.SignUpView.as_view(), name='sign_up'),
    # path('recover-password', views.RecoverPasswordView.as_view(), name='recover_password'),
    path('logout/', LogoutView.as_view(), {'next_page': settings.LOGOUT_REDIRECT_URL}, name='logout'),
    path('dashboard', views.DashboardView.as_view(), name='dashboard'),
    # path('profile', views.ProfileView.as_view(), name='profile'),
    path('redirect-session', views.redirect_session, name='redirect_session'),
    # path('list-competences/<int:id_test>/', views.get_competence_test, name='get_competence_test'),
]
