# Caja de Preguntas

Version Python: `3.8`

Version django: `3.2.9`

# External Dependencies

* [Docker](https://docs.docker.com/engine/install/)
* [Docker Compose](https://docs.docker.com/compose/install/)


#### Settings project

* Clone repository

```
$ git clone
$ cd florencia
```

* Build image docker

```
$ docker-compose build
```

* Up

```
$ docker-compose up
```

* Migrations

```
$ docker-compose run --rm app python manage.py makemigrations
$ docker-compose run --rm app python manage.py migrate
```

* Static files

```
$ docker-compose run --rm app ./manage.py collectstatic
```

* Create superuser

```
$ docker-compose run --rm app python manage.py createsuperuser
```


* Test

```
$ docker-compose run --rm app pytest -rA
```


Made with ♥ by [Jose Florez](www.joseflorez.co)
