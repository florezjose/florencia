from django.contrib.auth.models import User
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.urls import reverse


class ConfigModels(models.Model):
    """Contains default fields that could be included in future models"""
    created_at = models.DateTimeField(auto_now_add=True)
    created_by = models.IntegerField(null=True, blank=True)
    updated_at = models.DateTimeField(null=True, blank=True)
    updated_by = models.IntegerField(null=True, blank=True)

    class Meta:
        abstract = True


def validate_max_value_responses():
    return [MinValueValidator(0), MaxValueValidator(5)]


class QuestionBox(ConfigModels):
    title = models.CharField(verbose_name="Título del cuestionario", max_length=200)
    description = models.TextField(verbose_name="Descripción del cuestionario")
    active = models.BooleanField(verbose_name="Activar cuestionario?", default=False)
    slug = models.SlugField(null=True, unique=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('questions:questions_resolve', kwargs={'slug': self.slug})


class QuestionList(ConfigModels):
    question_box = models.ForeignKey(
        QuestionBox, on_delete=models.CASCADE,
        related_name="questions_list"
    )
    question = models.TextField(verbose_name="Ingrese la pregunta a realizar")


class Option(models.Model):
    question = models.ForeignKey(
        QuestionList, on_delete=models.CASCADE,
        related_name="options"
    )
    option = models.TextField(verbose_name="Ingrese la descripción de la opción de respuesta")
    value = models.IntegerField(
        verbose_name="Ingrese el valor (de 0 a 5) de dicha opción",
        validators=validate_max_value_responses()
    )


class AnswersUser(ConfigModels):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.user.username

    def create(self, username, email, password):
        User.objects.create_user(username, email, password)

    class Meta:
        verbose_name_plural = 'Respuestas por usuarios'
        verbose_name = 'Respuesta de usuario'


class AnswerQuestionList(models.Model):
    user = models.ForeignKey(AnswersUser, on_delete=models.CASCADE)
    question = models.ForeignKey(
        QuestionList, on_delete=models.CASCADE,
        related_name="answers_questions"
    )
    answer = models.ForeignKey(
        Option, on_delete=models.CASCADE,
        related_name="option_answer_question"
    )
