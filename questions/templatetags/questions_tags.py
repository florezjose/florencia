from django import template
from django.db.models import Count

from questions.models import AnswerQuestionList, QuestionList

register = template.Library()


@register.filter
def counter_questions(question):
    return question.questions_list.all().count()  # .answers_questions.all().count()


@register.filter
def counter_answers(question_id):
    question_count = QuestionList.objects.filter(question_box_id=question_id).count()
    answers = AnswerQuestionList.objects.filter(
        question__question_box_id=question_id
    ).annotate(dcount=Count('question__question_box_id')).count()
    return int(answers / question_count) if question_count > 0 else 0


@register.simple_tag
def aux_parent_account(count):
    return count


@register.simple_tag
def options_question(question):
    return question.options.all()
