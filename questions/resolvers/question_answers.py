from django.contrib import messages
from django.shortcuts import redirect, render
from django.urls import reverse_lazy

from questions.models import AnswersUser, AnswerQuestionList, QuestionBox


class QuestionGet:
    template_name = "questions/questions_resolve.html"

    @classmethod
    def resolve(cls, request, **kwargs):
        try:
            question = QuestionBox.objects.get(
                slug__exact=kwargs.get("slug"),
                active=True
            )
            context = {
                "question": question,
                "questions_list": question.questions_list.all(),
                "questions_list_count": question.questions_list.count(),
            }
            return render(request, cls.template_name, context)
        except Exception as err:
            return render(request, "questions/questions_404.html")


class QuestionPost:
    @staticmethod
    def resolve(request, **kwargs):
        data = request.POST
        id_question_box = data.get("id_question", None)
        if id_question_box:
            user_answer = AnswersUser.objects.get_or_create(user_id=1, name="Jose")[0]
            question_count = data.get("question_count")
            for i in range(1, int(question_count) + 1):
                id_question = data.get(f"id_question_{i}", None)
                value_question = data.get(f"option_question_{i}")
                AnswerQuestionList.objects.create(
                    user_id=user_answer.id, question_id=id_question, answer_id=value_question
                )

            messages.success(request, 'Formulario enviado exitosamente')

        return redirect(
            reverse_lazy(
                'questions:questions_resolve',
                kwargs={'slug': kwargs.get("slug")}
            )
        )


class QuestionAnswers:
    @staticmethod
    def resolve(request, action, **kwargs):
        resolve = {
            "get": QuestionGet,
            "post": QuestionPost
        }.get(action)

        return resolve.resolve(request, **kwargs)
