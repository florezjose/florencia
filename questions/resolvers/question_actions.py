from django.contrib import messages
from django.shortcuts import redirect
from django.urls import reverse_lazy

from questions.models import QuestionBox, QuestionList, Option
from questions.question_controller import QuestionController


class QuestionActive:
    @staticmethod
    def resolve(request, **kwargs):
        question_box = QuestionBox.objects.get(id=kwargs.get("id"))
        question_box.active = False if question_box.active else True
        question_box.save()
        messages.success(request, f"{question_box.title} actualizado exitosamente.")
        return redirect(reverse_lazy("questions:questions_box"))


class QuestionAdd:
    @staticmethod
    def resolve(request, **kwargs):
        data = request.POST
        title = data.get("title")
        slug = QuestionController.resolve_slug(title)
        description = data.get("description")
        question_count = int(data.get("counter"))
        box = QuestionBox.objects.create(
            title=title, description=description, slug=slug
        )

        options_list = list()
        for i in range(1, question_count + 1):
            title = data.get(f"question_{i}")
            question = QuestionList.objects.create(question_box=box, question=title)

            for j in range(0, 6):
                option = data.get(f"option_{j}_question_{i}")
                op = Option(option=option, value=j, question=question)
                options_list.append(op)

        Option.objects.bulk_create(options_list)

        messages.success(request, 'Cuestionario agregado exitosamente.')
        return redirect(reverse_lazy('questions:questions_box'))


class QuestionDelete:
    @staticmethod
    def resolve(request, **kwargs):
        try:
            data = request.POST
            id_q = data.get("id", None)
            action = kwargs.get("action")
            if id_q and action == "delete_b":
                QuestionBox.objects.get(id=id_q).delete()
                messages.success(request, 'Cuestionario eliminado exitosamente.')
            if id_q and action == "delete_q":
                q = QuestionList.objects.get(id=id_q)
                box_id = q.question_box_id
                q.delete()
                messages.success(request, 'Pregunta eliminada exitosamente.')
                return redirect(
                    reverse_lazy(
                        'questions:questions_action',
                        kwargs={'action': 'edit', 'id': box_id}
                    )
                )

        except Exception:
            messages.error(request, 'No hemos podido procesar tu solicitud. Inténtalo de nuevo.')

        return redirect(reverse_lazy('questions:questions_box'))


class QuestionEdit:
    @staticmethod
    def resolve_box_question(data):
        title = data.get("title")
        description = data.get("description")
        slug = QuestionController.resolve_slug(data.get("slug"), data.get("id", 0))
        active = True if data.get("active") else False

        question_box = QuestionBox.objects.get(id=int(data.get("id", 0)))
        is_update = False
        if question_box.title != title:
            question_box.title = title
            is_update = True
        if question_box.description != description:
            question_box.description = description
            is_update = True
        if question_box.slug != slug:
            question_box.slug = slug
            is_update = True
        if question_box.active != active:
            question_box.active = active
            is_update = True
        if is_update:
            question_box.save()

        return question_box

    @classmethod
    def resolve(cls, request, **kwargs):
        try:
            data = request.POST
            question_box = cls.resolve_box_question(data)

            question_count = int(data.get("counter"))
            for i in range(1, question_count + 1):
                id_question = data.get(f"id_question_{i}", None)
                if id_question:
                    question = QuestionList.objects.get(id=id_question)
                else:
                    question = QuestionList.objects.create(question_box=question_box, question="")

                if question.question != data.get(f"question_{i}"):
                    question.question = data.get(f"question_{i}")
                    question.save()

                for j in range(0, 6):
                    id_option = data.get(f"id_option_{j}_question_{i}", None)
                    if id_option:
                        option = Option.objects.get(id=id_option)
                    else:
                        option = Option.objects.create(question=question, value=j, option="")

                    if option.option != data.get(f"option_{j}_question_{i}"):
                        option.option = data.get(f"option_{j}_question_{i}")
                        option.save()

            messages.success(request, 'Formulario actualizado correctamente.')

        except Exception as err:
            messages.error(request, f'No hemos podido procesar tu solicitud. Inténtalo de nuevo. {err}')

        return redirect(
            reverse_lazy(
                'questions:questions_action',
                kwargs={'action': 'edit', 'id': kwargs.get("id")}
            )
        )


class QuestionAction:
    @staticmethod
    def resolve(request, **kwargs):
        action = {
            "add": QuestionAdd,
            "active": QuestionActive,
            "edit": QuestionEdit,
            "delete_b": QuestionDelete,
            "delete_q": QuestionDelete,
        }.get(kwargs.get("action"))

        return action.resolve(request, **kwargs)
