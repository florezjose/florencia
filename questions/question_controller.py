import random
import string

from django.template.defaultfilters import slugify

from questions.models import QuestionList, QuestionBox


class QuestionController:

    @classmethod
    def get_data_question_list(cls, id_q):
        question = QuestionList.objects.get(id=id_q)
        question.title = question.question
        return {
            "title": question.question,
            "id": question.id,
            "id_edit": question.question_box_id,
        }

    @classmethod
    def get_data_question_box(cls, id_q):
        question = QuestionBox.objects.get(id=id_q)
        return {
            "title": question.title,
            "id": question.id,
            "id_edit": question.id,
        }

    @classmethod
    def get_data_question_edit(cls, id_q):
        question = QuestionBox.objects.get(id=id_q)
        questions_list = question.questions_list.all()
        return {
            "question": question,
            "questions_list": questions_list,
            "questions_list_count": questions_list.count(),
        }

    @staticmethod
    def get_questions_box():
        return {
            "questions": QuestionBox.objects.all().order_by('created_at'),
        }

    @classmethod
    def get_statistics(cls, context):
        data_q = dict()
        count, total = 0, 0
        question_list = context.get("question").questions_list.all()
        for q in question_list:
            answers = q.answers_questions.all()
            for a in answers:
                if data_q and q.question in data_q and \
                        f"{a.answer.value}" in data_q[q.question]:
                    data_q[q.question][f"{a.answer.value}"]["value"] += 1
                else:
                    if not q.question in data_q:
                        data_q[q.question] = {}

                    data_q[q.question][f"{a.answer.value}"] = {
                        "value": 1,
                        "description": a.answer.option
                    }
                count += 1
                total += a.answer.value

        context["data_q"] = data_q
        context["average"] = "{:,.1f}".format(float(total / count if count > 0 else 0))

        return context

    @staticmethod
    def resolve_slug(title, id_q):
        slug = slugify(title)
        if QuestionBox.objects.filter(slug__exact=slug).exclude(id=id_q).exists():
            result = ''.join((random.choice(string.ascii_lowercase) for x in range(10)))
            slug = slug + result
        return slug[:50]
