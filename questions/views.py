from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.views import View

from questions.question_controller import QuestionController
from questions.resolvers.question_actions import QuestionAction
from questions.resolvers.question_answers import QuestionAnswers


class QuestionActions(LoginRequiredMixin, View):
    template_name = "questions/questions_box.html"
    actions = ["add", "edit", "statistics", "delete_q", "delete_b"]

    def get(self, request, *args, **kwargs):
        action = kwargs.get("action")
        if action in self.actions:
            file = action.replace("_q", "")
            file = file.replace("_b", "")
            self.template_name = f"questions/questions_{file}.html"

        if kwargs.get("id"):
            if action == "delete_q":
                context = QuestionController.get_data_question_list(
                    kwargs.get("id")
                )
            elif action == "delete_b":
                context = QuestionController.get_data_question_box(
                    kwargs.get("id")
                )
            else:
                context = QuestionController.get_data_question_edit(kwargs.get("id"))

                if action == "statistics":
                    QuestionController.get_statistics(context)
        else:
            context = QuestionController.get_questions_box()

        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        return QuestionAction.resolve(request, **kwargs)


class QuestionResolve(View):
    def get(self, request, *args, **kwargs):
        return QuestionAnswers.resolve(request, 'get', **kwargs)

    def post(self, request, *args, **kwargs):
        return QuestionAnswers.resolve(request, 'post', **kwargs)
