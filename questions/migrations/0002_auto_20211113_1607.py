# Generated by Django 3.2.9 on 2021-11-13 16:07

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('questions', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='answerquestionlist',
            name='answer',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='option_answer_question', to='questions.option'),
        ),
        migrations.AlterField(
            model_name='answerquestionlist',
            name='question',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='answers_questions', to='questions.questionlist'),
        ),
        migrations.AlterField(
            model_name='option',
            name='question',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='options', to='questions.questionlist'),
        ),
        migrations.AlterField(
            model_name='questionlist',
            name='question_box',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='questions_list', to='questions.questionbox'),
        ),
    ]
