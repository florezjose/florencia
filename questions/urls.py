from django.urls import path

from questions import views

app_name = 'questions'
urlpatterns = [
    path('resolve/<slug:slug>/', views.QuestionResolve.as_view(), name='questions_resolve'),
    path('questions/', views.QuestionActions.as_view(), name='questions_box'),
    path('questions/<str:action>/', views.QuestionActions.as_view(), name='questions_add'),
    path('questions/<str:action>/<int:id>/', views.QuestionActions.as_view(), name='questions_action'),
]
